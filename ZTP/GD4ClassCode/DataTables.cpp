#include "DataTables.hpp"
#include "Aircraft.hpp"
#include "Projectile.hpp"
#include "Pickup.hpp"
#include "Particle.hpp"
//this is like appData from 3D, presets for game objects
// For std::bind() placeholders _1, _2, ...
using namespace std::placeholders;

std::vector<AircraftData> initializeAircraftData()
{
	std::vector<AircraftData> data(Aircraft::TypeCount);

	data[Aircraft::Osprey].hitpoints = 100;
	data[Aircraft::Osprey].speed = 200.f;
	data[Aircraft::Osprey].fireInterval = sf::seconds(1);
	data[Aircraft::Osprey].texture = Textures::Entities;
	data[Aircraft::Osprey].textureRect = sf::IntRect(95, 104, 50, 50);
	data[Aircraft::Osprey].hasRollAnimation = false;
	//Find and change tags: Eagle, Raptor, Avenger, AlliedBullet, EnemyBullet 
	//HealthRefill = RepairPack, MissileAmmo = MissilePack, FireSpread = EnhancedLasers
	//FireRate = BurstLasers
	data[Aircraft::Shrike].hitpoints = 100;
	data[Aircraft::Shrike].speed = 200.f;
	data[Aircraft::Shrike].fireInterval = sf::seconds(1);
	data[Aircraft::Shrike].texture = Textures::Entities;
	data[Aircraft::Shrike].textureRect = sf::IntRect(50, 104, 45, 101);
	data[Aircraft::Shrike].hasRollAnimation = false;

	data[Aircraft::Marauder].hitpoints = 20;
	data[Aircraft::Marauder].speed = 80.f;
	data[Aircraft::Marauder].texture = Textures::Entities;
	data[Aircraft::Marauder].textureRect = sf::IntRect(60, 0, 84, 84);
	data[Aircraft::Marauder].directions.push_back(Direction(+45.f, 80.f));
	data[Aircraft::Marauder].directions.push_back(Direction(-45.f, 160.f));
	data[Aircraft::Marauder].directions.push_back(Direction(+45.f, 80.f));
	data[Aircraft::Marauder].fireInterval = sf::Time::Zero;
	data[Aircraft::Marauder].hasRollAnimation = false;

	data[Aircraft::Ravager].hitpoints = 40;
	data[Aircraft::Ravager].speed = 50.f;
	data[Aircraft::Ravager].texture = Textures::Entities;
	data[Aircraft::Ravager].textureRect = sf::IntRect(0, 0, 60, 104);
	data[Aircraft::Ravager].directions.push_back(Direction(+45.f, 50.f));
	data[Aircraft::Ravager].directions.push_back(Direction(0.f, 50.f));
	data[Aircraft::Ravager].directions.push_back(Direction(-45.f, 100.f));
	data[Aircraft::Ravager].directions.push_back(Direction(0.f, 50.f));
	data[Aircraft::Ravager].directions.push_back(Direction(+45.f, 50.f));
	data[Aircraft::Ravager].fireInterval = sf::seconds(2);
	data[Aircraft::Ravager].hasRollAnimation = false;

	data[Aircraft::DeathSquid].hitpoints = 40;
	data[Aircraft::DeathSquid].speed = 50.f;
	data[Aircraft::DeathSquid].texture = Textures::Entities;
	data[Aircraft::DeathSquid].textureRect = sf::IntRect(144, 0, 60, 104);
	data[Aircraft::DeathSquid].directions.push_back(Direction(+45.f, 50.f));
	data[Aircraft::DeathSquid].directions.push_back(Direction(0.f, 50.f));
	data[Aircraft::DeathSquid].directions.push_back(Direction(-45.f, 100.f));
	data[Aircraft::DeathSquid].directions.push_back(Direction(0.f, 50.f));
	data[Aircraft::DeathSquid].directions.push_back(Direction(+45.f, 50.f));
	data[Aircraft::DeathSquid].fireInterval = sf::seconds(2);
	data[Aircraft::DeathSquid].hasRollAnimation = false;
	return data;
}

std::vector<ProjectileData> initializeProjectileData()
{
	std::vector<ProjectileData> data(Projectile::TypeCount);

	data[Projectile::Zap].damage = 10;
	data[Projectile::Zap].speed = 300.f;
	data[Projectile::Zap].texture = Textures::Entities;
	data[Projectile::Zap].textureRect = sf::IntRect(160, 104, 12, 25);

	data[Projectile::EnemyZap].damage = 10;
	data[Projectile::EnemyZap].speed = 300.f;
	data[Projectile::EnemyZap].texture = Textures::Entities;
	data[Projectile::EnemyZap].textureRect = sf::IntRect(204, 0, 10, 20);

	data[Projectile::Missile].damage = 200;
	data[Projectile::Missile].speed = 150.f;
	data[Projectile::Missile].texture = Textures::Entities;
	data[Projectile::Missile].textureRect = sf::IntRect(145, 104, 15, 30);

	return data;
}

std::vector<PickupData> initializePickupData()
{
	std::vector<PickupData> data(Pickup::TypeCount);

	data[Pickup::RepairPack].texture = Textures::Entities;
	data[Pickup::RepairPack].textureRect = sf::IntRect(135, 168, 40, 40);
	data[Pickup::RepairPack].action = [](Aircraft& a) { a.repair(25); };

	data[Pickup::MissilePack].texture = Textures::Entities;
	data[Pickup::MissilePack].textureRect = sf::IntRect(95, 168, 40, 40);
	data[Pickup::MissilePack].action = std::bind(&Aircraft::collectMissiles, _1, 3);

	data[Pickup::EnhancedLasers].texture = Textures::Entities;
	data[Pickup::EnhancedLasers].textureRect = sf::IntRect(175, 168, 40, 40);
	data[Pickup::EnhancedLasers].action = std::bind(&Aircraft::increaseSpread, _1);

	data[Pickup::BurstLasers].texture = Textures::Entities;
	data[Pickup::BurstLasers].textureRect = sf::IntRect(205, 168, 40, 40);
	data[Pickup::BurstLasers].action = std::bind(&Aircraft::increaseFireRate, _1);

	return data;
}

std::vector<ParticleData> initializeParticleData()
{

	std::vector<ParticleData> data(Particle::ParticleCount);

	data[Particle::Propellant].color = sf::Color(255, 255, 50);
	data[Particle::Propellant].lifetime = sf::seconds(0.6f);

	data[Particle::Smoke].color = sf::Color(50, 50, 50);
	data[Particle::Smoke].lifetime = sf::seconds(4.f);

	return data;
}

