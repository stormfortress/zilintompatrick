What is this repository for?
Patrick Collins, Zilin Feng, and Thomas Hanahoe
Repo for dev work on StormFortress, the space shmup game. 
*V.1.0 = Current version: Space combat game with 2 players, built by Tom and Zilin while Patrick picked up the pieces after GitSplosion!
V.0.8 = Patrick replaced all image assets in game so it's a space game with 4 player types and 3 enemy types
V.0.75 = Zilin implemented changed title screen, new audio, and 2nd Player::OLD2Person Folder
V.0.5 = base code from Lecturer annotated by Patrick Collins for team reference

How do I get set up?
Download the contents of Repo. Open solution in VS 15 or 17. Download and extract SFML 2.3.2 32bit for VS15. Note it's location. Set Includes etc in project properties pointing to SFML folders. Debug project to run game
SFML Setup Tutorial:
https://youtu.be/_9yem5dJt2E

Contribution guidelines
N/A

Who do I talk to?
*Any team members Zilin, Tom, Patrick

Controls:
Player 1 Movement
W = up 
S = down
A = right
D = left
F = shoot guns
R = shoot missile
Player 2 Movement
-> (arrow) keys
spacebar = shoot guns
M = shoot missile